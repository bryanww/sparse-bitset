plugins {
    java
    kotlin("jvm") version "1.3.61"
}

group = "com.arcrocket"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.slf4j:slf4j-simple:1.7.30")
    implementation("io.github.microutils:kotlin-logging:1.7.8")
    implementation("com.google.guava:guava:23.0")
    implementation("com.zaxxer:SparseBitSet:1.2")
    implementation("org.roaringbitmap:RoaringBitmap:0.8.13")
    testCompile("junit", "junit", "4.12")
}

tasks.test {
    minHeapSize = "4g"
    maxHeapSize = "4g"
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_11
}
tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "11"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "11"
    }
}