package com.arcrocket

import com.arcrocket.utils.BitSetUtils
import com.arcrocket.utils.TimeUtils
import mu.KLogging
import org.junit.Test
import java.util.*

class ExperimentalTests {

  @Test
  fun testArrayComparison() {
    val name = "testArrayComparison"
    // comparing arrays of long values is faster than comparing arrays of int values on x86-64 because of 64-bit word sizes
    val random = Random()
    val ints0 = IntArray(20_000_000) { random.nextInt() }
    val ints1 = ints0.copyOf()
    val longs0 = BitSetUtils.toLongArray(ints0)
    val longs1 = longs0.copyOf()
    logger.info { "$name: ints0=${ints0.slice(0..3)}, ints1=${ints1.slice(0..3)}" }
    logger.info { "$name: longs0=${longs0.slice(0..1)}, longs1=${longs1.slice(0..1)}" }
    val intFromLong00 = (longs0[0] ushr 32).toInt()
    val intFromLong01 = (longs0[0] and 0xffffffff).toInt()
    logger.info { "$name: intFromLong00=$intFromLong00, intFromLong01=$intFromLong01" +
        ", same=${intFromLong00 == intFromLong01}, same int0=${intFromLong00 == ints0[0]}, same int1=${intFromLong01 == ints0[1]}" }

    measureAndPrint("$name: intsContentEquals") { ints0.contentEquals(ints1) }
    measureAndPrint("$name: longsContentEquals") { longs0.contentEquals(longs1) }
  }

  @Test
  fun testDeltas() {
    val name = "testDeltas"
    val path = BitSetUtils.ENWIK8_PATH
    val words = measure("$name: loaded \"$path\"") {
      BitSetUtils.readWords(path)
    }
    logger.info { "$name: words.size=${words.size}" }

    val hashes = measure("$name: finished hashing") {
      BitSetUtils.buildHashes(words)
    }

    val sortedHashes = measure("$name: finished sorting") {
      hashes.sorted()
    }

    val deltas = measure("$name: finished deltas") {
      val deltas = IntArray(sortedHashes.size - 1)
      for (i in 1 until sortedHashes.size) {
        deltas[i - 1] = sortedHashes[i] - sortedHashes[i - 1]
      }
      deltas.filter { it != 0 }
    }
    logger.info { "$name; deltas: size=${deltas.size}, min=${deltas.min()}, max=${deltas.max()}, average=${deltas.average()}" }
  }

  private fun <T: Any> measure(message: String, block: () -> T): T {
    val result = TimeUtils.resultWithMillis(block)
    System.gc()
    BitSetImplementationTests.logger.info { "$message: millis=${result.millis}, memoryInfo=${BitSetUtils.memoryInfo}" }
    return result.result
  }

  private fun <T: Any> measureAndPrint(message: String, block: () -> T): T {
    val result = measure(message, block)
    BitSetImplementationTests.logger.info { "$message: result=$result" }
    return result
  }

  companion object : KLogging()
}