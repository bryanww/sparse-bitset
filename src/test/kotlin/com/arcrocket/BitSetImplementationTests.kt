package com.arcrocket

import com.arcrocket.utils.BitSetUtils
import com.arcrocket.utils.TimeUtils
import mu.KLogging
import org.junit.Test

/**
 * `enwik8` and `enwik9` can be downloaded here: http://mattmahoney.net/dc/textdata.html
 */
class BitSetImplementationTests {

  @Test
  fun testSparseMobyDick() = testSparse("testSparseMobyDick",  BitSetUtils.MOBY_DICK_PATH)

  @Test
  fun testSparseBigTxt() = testSparse("testSparseBigTxt", BitSetUtils.BIG_TXT_PATH)

  @Test
  fun testSparseEnwik8() = testSparse("testSparseEnwik8", BitSetUtils.ENWIK8_PATH)

  @Test
  fun testZaxxerMobyDick() = testZaxxer("testZaxxerMobyDick", BitSetUtils.MOBY_DICK_PATH)

  @Test
  fun testZaxxerBigTxt() = testZaxxer("testZaxxerTxt", BitSetUtils.BIG_TXT_PATH)

  @Test
  fun testZaxxerEnwik8() = testZaxxer("testZaxxerEnwik8", BitSetUtils.ENWIK8_PATH)

  @Test
  fun testRoaringMobyDick() = testRoaring("testRoaringMobyDick", BitSetUtils.MOBY_DICK_PATH)

  @Test
  fun testRoaringBigTxt() = testRoaring("testRoaringTxt", BitSetUtils.BIG_TXT_PATH)

  @Test
  fun testRoaringEnwik8() = testRoaring("testRoaringEnwik8", BitSetUtils.ENWIK8_PATH)

  private fun testSparse(name: String, path: String) = test(name, path) { hashes ->
    BitSetUtils.buildSparseBitSet(hashes)
  }

  private fun testZaxxer(name: String, path: String) = test(name, path) { hashes ->
    BitSetUtils.buildZaxxerBitSet(hashes)
  }

  private fun testRoaring(name: String, path: String) = test(name, path) { hashes ->
    BitSetUtils.buildRoaringBitSet(hashes)
  }

  private fun test(name: String, path: String, test: (IntArray) -> Any) {
    val words = measure("$name: loaded \"$path\"") {
      BitSetUtils.readWords(path)
    }
    logger.info { "$name: words.size=${words.size}" }

    System.gc()
    val hashes = measure("$name: finished hashing") {
      BitSetUtils.buildHashes(words)
    }
    logger.info { "$name: hashes.size=${hashes.size}" }

    System.gc()
    logger.info { "$name: running test: memoryInfo=${BitSetUtils.memoryInfo}" }
    measureAndPrint("$name: finished building") {
      test(hashes)
    }
  }

  private fun <T: Any> measure(message: String, block: () -> T): T {
    val result = TimeUtils.resultWithMillis(block)
    System.gc()
    logger.info { "$message: millis=${result.millis}, memoryInfo=${BitSetUtils.memoryInfo}" }
    return result.result
  }

  private fun <T: Any> measureAndPrint(message: String, block: () -> T): T {
    val result = measure(message, block)
    logger.info { "$message: result=$result" }
    return result
  }

  companion object : KLogging()
}