package com.arcrocket

import com.arcrocket.utils.BitSetUtils
import org.junit.Test

class ArrayConversionTests {

  @Test
  fun testSize0() {
    val longArray = BitSetUtils.toLongArray(intArrayOf())
    val intArray = BitSetUtils.toIntArray(longArrayOf())
    assert(longArray.isEmpty())
    assert(intArray.isEmpty())
  }

  @Test
  fun testSize1() {
    val longArray = BitSetUtils.toLongArray(intArrayOf(9))
    val intArray = BitSetUtils.toIntArray(longArrayOf(
      (1L shl 32) or 9L
    ))
    assert(longArray.size == 1)
    assert(intArray.size == 2)
    assert(longArray[0] == 9L shl 32)
    assert(intArray[0] == 1)
    assert(intArray[1] == 9)
  }

  @Test
  fun testSize2() {
    val longArray = BitSetUtils.toLongArray(intArrayOf(9, 8))
    val intArray = BitSetUtils.toIntArray(longArrayOf(
      (1L shl 32) or 9L,
      (2L shl 32) or 8L
    ))
    assert(longArray.size == 1)
    assert(intArray.size == 4)
    assert(longArray[0] == (9L shl 32) or 8L)
    assert(intArray[0] == 1)
    assert(intArray[1] == 9)
    assert(intArray[2] == 2)
    assert(intArray[3] == 8)
  }

  @Test
  fun testSize3() {
    val longArray = BitSetUtils.toLongArray(intArrayOf(9, 8, 7))
    val intArray = BitSetUtils.toIntArray(longArrayOf(
      (1L shl 32) or 9L,
      (2L shl 32) or 8L,
      (3L shl 32) or 7L
    ))
    assert(longArray.size == 2)
    assert(intArray.size == 6)
    assert(longArray[0] == (9L shl 32) or 8L)
    assert(longArray[1] == 7L shl 32)
    assert(intArray[0] == 1)
    assert(intArray[1] == 9)
    assert(intArray[2] == 2)
    assert(intArray[3] == 8)
    assert(intArray[4] == 3)
    assert(intArray[5] == 7)
  }

  @Test
  fun testSize4() {
    val longArray = BitSetUtils.toLongArray(intArrayOf(9, 8, 7, 6))
    val intArray = BitSetUtils.toIntArray(longArrayOf(
      (1L shl 32) or 9L,
      (2L shl 32) or 8L,
      (3L shl 32) or 7L,
      (4L shl 32) or 6L
    ))
    assert(longArray.size == 2)
    assert(intArray.size == 8)
    assert(longArray[0] == (9L shl 32) or 8L)
    assert(longArray[1] == (7L shl 32) or 6L)
    assert(intArray[0] == 1)
    assert(intArray[1] == 9)
    assert(intArray[2] == 2)
    assert(intArray[3] == 8)
    assert(intArray[4] == 3)
    assert(intArray[5] == 7)
    assert(intArray[6] == 4)
    assert(intArray[7] == 6)
  }

  @Test
  fun testSize5() {
    val longArray = BitSetUtils.toLongArray(intArrayOf(9, 8, 7, 6, 5))
    val intArray = BitSetUtils.toIntArray(longArrayOf(
      (1L shl 32) or 9L,
      (2L shl 32) or 8L,
      (3L shl 32) or 7L,
      (4L shl 32) or 6L,
      (5L shl 32) or 4L
    ))
    assert(longArray.size == 3)
    assert(intArray.size == 10)
    assert(longArray[0] == (9L shl 32) or 8L)
    assert(longArray[1] == (7L shl 32) or 6L)
    assert(longArray[2] == 5L shl 32)
    assert(intArray[0] == 1)
    assert(intArray[1] == 9)
    assert(intArray[2] == 2)
    assert(intArray[3] == 8)
    assert(intArray[4] == 3)
    assert(intArray[5] == 7)
    assert(intArray[6] == 4)
    assert(intArray[7] == 6)
    assert(intArray[8] == 5)
    assert(intArray[9] == 4)
  }

  @Test
  fun testSize6() {
    val longArray = BitSetUtils.toLongArray(intArrayOf(9, 8, 7, 6, 5, 4))
    val intArray = BitSetUtils.toIntArray(longArrayOf(
      (1L shl 32) or 9L,
      (2L shl 32) or 8L,
      (3L shl 32) or 7L,
      (4L shl 32) or 6L,
      (5L shl 32) or 4L,
      (6L shl 32) or 3L
    ))
    assert(longArray.size == 3)
    assert(intArray.size == 12)
    assert(longArray[0] == (9L shl 32) or 8L)
    assert(longArray[1] == (7L shl 32) or 6L)
    assert(longArray[2] == (5L shl 32) or 4L)
    assert(intArray[0] == 1)
    assert(intArray[1] == 9)
    assert(intArray[2] == 2)
    assert(intArray[3] == 8)
    assert(intArray[4] == 3)
    assert(intArray[5] == 7)
    assert(intArray[6] == 4)
    assert(intArray[7] == 6)
    assert(intArray[8] == 5)
    assert(intArray[9] == 4)
    assert(intArray[10] == 6)
    assert(intArray[11] == 3)
  }
}