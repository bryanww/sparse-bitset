package com.arcrocket

import com.arcrocket.utils.BitSetUtils
import com.arcrocket.utils.TimeUtils
import mu.KLogging
import org.junit.Test

/**
 * `enwik8` and `enwik9` can be downloaded here: http://mattmahoney.net/dc/textdata.html
 */
class BitSetEqualsTests {

  @Test
  fun testZaxxerEquals() {
    val name = "testZaxxerEquals"
    val bitset0 = BitSetUtils.buildZaxxerBitSetEnwik8()
    val bitset1 = BitSetUtils.buildZaxxerBitSetEnwik8()

    measureAndPrint("$name: equals") { bitset0 == bitset1 }
    measureAndPrint("$name: contentDeepEquals") { bitset0.contentDeepEquals(bitset1) }

    measure("$name: equals x $ITERATIONS") {
      repeat(ITERATIONS) { bitset0 == bitset1 }
    }
  }

  @Test
  fun testZaxxerEqualsByWordsAndIndexDeltas() {
    val name = "testZaxxerEqualsByWordsAndIndexDeltas"
    val bitset0 = BitSetUtils.buildZaxxerBitSetEnwik8()
    val bitset1 = BitSetUtils.buildZaxxerBitSetEnwik8()

    measureAndPrint("$name: buildWordsAndIndexDeltas0") {
      bitset0.buildWordsAndIndexDeltas()
      bitset0.descWordsAndIndexDeltas
    }
    measureAndPrint("$name: buildWordsAndIndexDeltas1") {
      bitset1.buildWordsAndIndexDeltas()
      bitset1.descWordsAndIndexDeltas
    }
    measureAndPrint("$name: equalsByWordsAndIndexDeltas") {
      bitset0.equalsByWordsAndIndexDeltas(bitset1)
    }

    // modify, rebuild, and check (not equal)
    measureAndPrint("$name: flipThenBuildWordsAndIndexDeltas") {
      bitset0.flip(1024 * 1024)
      bitset0.buildWordsAndIndexDeltas()
      bitset0.descWordsAndIndexDeltas
    }
    measureAndPrint("$name: afterFlipEqualsByWordsAndIndexDeltas") {
      bitset0.equalsByWordsAndIndexDeltas(bitset1)
    }

    // modify, rebuild, and check (equal)
    measureAndPrint("$name: unflipThenBuildWordsAndIndexDeltas") {
      bitset0.flip(1024 * 1024)
      bitset0.buildWordsAndIndexDeltas()
      bitset0.descWordsAndIndexDeltas
    }
    measureAndPrint("$name: afterUnflipEqualsByWordsAndIndexDeltas") {
      bitset0.equalsByWordsAndIndexDeltas(bitset1)
    }

    measure("$name: equalsByWordsAndIndexDeltas x $ITERATIONS") {
      repeat(ITERATIONS) { bitset0.equalsByWordsAndIndexDeltas(bitset1) }
    }
  }

  @Test
  fun testZaxxerEqualsByPackedDeltas() {
    val name = "testZaxxerEqualsByPackedDeltas"
    val bitset0 = BitSetUtils.buildZaxxerBitSetEnwik8()
    val bitset1 = BitSetUtils.buildZaxxerBitSetEnwik8()

    measureAndPrint("$name: buildPackedDeltas0") {
      bitset0.buildPackedDeltas()
      bitset0.descPackedDeltas
    }
    measureAndPrint("$name: buildPackedDeltas1") {
      bitset1.buildPackedDeltas()
      bitset1.descPackedDeltas
    }
    measureAndPrint("$name: equalsByPackedDeltas") {
      bitset0.equalsByPackedDeltas(bitset1)
    }

    // modify, rebuild, and check (not equal)
    measureAndPrint("$name: flipThenBuildPackedDeltas") {
      bitset0.flip(1024 * 1024)
      bitset0.buildPackedDeltas()
      bitset0.descPackedDeltas
    }
    measureAndPrint("$name: afterFlipEqualsByPackedDeltas") {
      bitset0.equalsByPackedDeltas(bitset1)
    }

    // modify, rebuild, and check (equal)
    measureAndPrint("$name: unflipThenBuildPackedDeltas") {
      bitset0.flip(1024 * 1024)
      bitset0.buildPackedDeltas()
      bitset0.descPackedDeltas
    }
    measureAndPrint("$name: afterUnflipEqualsByPackedDeltas") {
      bitset0.equalsByPackedDeltas(bitset1)
    }

    measure("$name: equalsByPackedDeltas x $ITERATIONS") {
      repeat(ITERATIONS) { bitset0.equalsByPackedDeltas(bitset1) }
    }
  }

  @Test
  fun testRoaringEquals() {
    val name = "testRoaringEquals"
    val bitset0 = BitSetUtils.buildRoaringBitSetEnwik8()
    val bitset1 = BitSetUtils.buildRoaringBitSetEnwik8()

    measureAndPrint("$name: equals") { bitset0 == bitset1 }

    measure("$name: equals x $ITERATIONS") {
      repeat(ITERATIONS) { bitset0 == bitset1 }
    }
  }

  @Test
  fun testRoaringEqualsByPackedDeltas() {
    val name = "testRoaringEqualsByPackedDeltas"
    val bitset0 = BitSetUtils.buildRoaringBitSetEnwik8()
    val bitset1 = BitSetUtils.buildRoaringBitSetEnwik8()

    measureAndPrint("$name: buildPackedDeltas0") {
      bitset0.buildPackedDeltas()
      bitset0.descPackedDeltas
    }
    measureAndPrint("$name: buildPackedDeltas1") {
      bitset1.buildPackedDeltas()
      bitset1.descPackedDeltas
    }
    measureAndPrint("$name: equalsByPackedDeltas") {
      bitset0.equalsByPackedDeltas(bitset1)
    }

    // modify, rebuild, and check (not equal)
    measureAndPrint("$name: flipThenBuildPackedDeltas") {
      bitset0.flip(1024 * 1024)
      bitset0.buildPackedDeltas()
      bitset0.descPackedDeltas
    }
    measureAndPrint("$name: afterFlipEqualsByPackedDeltas") {
      bitset0.equalsByPackedDeltas(bitset1)
    }

    // modify, rebuild, and check (equal)
    measureAndPrint("$name: unflipThenBuildPackedDeltas") {
      bitset0.flip(1024 * 1024)
      bitset0.buildPackedDeltas()
      bitset0.descPackedDeltas
    }
    measureAndPrint("$name: afterUnflipEqualsByPackedDeltas") {
      bitset0.equalsByPackedDeltas(bitset1)
    }

    measure("$name: equalsByPackedDeltas x $ITERATIONS") {
      repeat(ITERATIONS) { bitset0.equalsByPackedDeltas(bitset1) }
    }
  }

  private fun <T: Any> measure(message: String, block: () -> T): T {
    val result = TimeUtils.resultWithMillis(block)
    System.gc()
    BitSetImplementationTests.logger.info { "$message: millis=${result.millis}, memoryInfo=${BitSetUtils.memoryInfo}" }
    return result.result
  }

  private fun <T: Any> measureAndPrint(message: String, block: () -> T): T {
    val result = measure(message, block)
    BitSetImplementationTests.logger.info { "$message: result=$result" }
    return result
  }

  companion object : KLogging() {
    const val ITERATIONS = 1000
  }
}