package com.arcrocket.utils

import com.arcrocket.RoaringBitSet
import com.arcrocket.SparseBitSet
import com.arcrocket.ZaxxerBitSet
import com.google.common.hash.Hashing
import org.roaringbitmap.RoaringBitmap
import java.io.File

object BitSetUtils {
  const val ENWIK8_PATH = "src/test/resources/enwik8"
  const val ENWIK9_PATH = "src/test/resources/enwik9"
  const val MOBY_DICK_PATH = "src/test/resources/mobydick.txt"
  const val BIG_TXT_PATH = "src/test/resources/big.txt"

  fun buildSparseBitSetEnwik8() = buildSparseBitSet(ENWIK8_PATH)
  fun buildZaxxerBitSetEnwik8() = buildZaxxerBitSet(ENWIK8_PATH)
  fun buildRoaringBitSetEnwik8() = buildRoaringBitSet(ENWIK8_PATH)

  fun buildSparseBitSetEnwik9() = buildSparseBitSet(ENWIK9_PATH)
  fun buildZaxxerBitSetEnwik9() = buildZaxxerBitSet(ENWIK9_PATH)
  fun buildRoaringBitSetEnwik9() = buildRoaringBitSet(ENWIK9_PATH)

  fun buildSparseBitSetBigTxt() = buildSparseBitSet(BIG_TXT_PATH)
  fun buildZaxxerBitSetBigTxt() = buildZaxxerBitSet(BIG_TXT_PATH)
  fun buildRoaringBitSetBigTxt() = buildRoaringBitSet(BIG_TXT_PATH)

  fun buildSparseBitSetMobyDick() = buildSparseBitSet(MOBY_DICK_PATH)
  fun buildZaxxerBitSetMobyDick() = buildZaxxerBitSet(MOBY_DICK_PATH)
  fun buildRoaringBitSetMobyDick() = buildRoaringBitSet(MOBY_DICK_PATH)

  fun buildSparseBitSet(path: String) = buildSparseBitSet(buildHashes(readWords(path)))
  fun buildZaxxerBitSet(path: String) = buildZaxxerBitSet(buildHashes(readWords(path)))
  fun buildRoaringBitSet(path: String) = buildRoaringBitSet(buildHashes(readWords(path)))

  fun readWords(path: String): Array<String> = File(path)
    .readText(Charsets.UTF_8)
    .split(Regex("\\s+"))
    .filter { it.isNotEmpty() }
    .map { it.toLowerCase() }
    .toTypedArray()

  fun buildHashes(words: Array<String>): IntArray {
    val hashFunction = Hashing.murmur3_32()
    return words.map {
      hashFunction.hashUnencodedChars(it).asInt()
    }.toIntArray()
  }

  fun buildSparseBitSet(hashes: IntArray): SparseBitSet {
    val pagesPerAllocation = (hashes.size * 0.15f).toInt()
    val bitset = SparseBitSet(pagesPerAllocation)
    hashes.forEach { bitset.set(it, true) }
    bitset.updateCardinality()
    return bitset
  }

  fun buildZaxxerBitSet(hashes: IntArray): ZaxxerBitSet {
    val bitset = ZaxxerBitSet()
    hashes.forEach {
      // library doesn't handle negative ints; this changes cardinality slightly
      //val positiveHash = (it.inv() + 1) ushr 1
      val positiveHash = if (it < 0) -it else it  // no difference; CMOV
      bitset.set(positiveHash, true)
    }
    return bitset
  }

  fun buildRoaringBitSet(hashes: IntArray): RoaringBitSet {
    val bitset = RoaringBitSet()
    hashes.forEach { bitset.add(it) }
    return bitset
  }

  fun toLongArray(intArray: IntArray): LongArray {
    val outputSize = intArray.size / 2 + (if (intArray.size % 2 == 1) 1 else 0)
    val longArray = LongArray(outputSize)
    var i = 0; var j = 0
    val inputSize = intArray.count()
    repeat(outputSize) {
      val a = intArray[j].toLong()
      val b = if (j + 1 == inputSize) 0L else intArray[j + 1].toLong()
      longArray[i] = (a shl 32) or b
      ++i
      j += 2
    }
    return longArray
  }

  fun toIntArray(longArray: LongArray): IntArray {
    val intArray = IntArray(longArray.size * 2)
    var i = 0; var j = 0
    repeat(longArray.count()) {
      intArray[i] = (longArray[j] ushr 32).toInt()
      intArray[i + 1] = (longArray[j] and 0xffffffff).toInt()
      i += 2
      ++j
    }
    return intArray
  }

  val memoryInfo: String get() {
    val rt = Runtime.getRuntime()
    val totalMB = rt.totalMemory().toFloat() / (1024 * 1024)
    val usedMB = (rt.totalMemory() - rt.freeMemory()).toFloat() / (1024 * 1024)
    return "{usedMB=%.1f, totalMB=%.1f}".format(usedMB, totalMB)
  }
}