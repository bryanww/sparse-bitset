package com.arcrocket

import com.arcrocket.utils.TimeUtils
import mu.KLogging
import java.lang.Integer.max
import java.nio.ByteBuffer
import java.nio.IntBuffer
import java.nio.LongBuffer
import java.nio.ShortBuffer

/**
 * Implementation of a sparse bitset that uses memory arenas.
 * Uses NIO and `allocateDirect`. Replace these calls with `allocate` to see the memory counted on the JVM heap.
 */
class SparseBitSet(pagesPerAllocation: Int = 1024) {
  internal val level1Table = ByteBuffer.allocateDirect(LEVEL1_SIZE * 4).asIntBuffer()
  internal val level2Tables = Tables(LEVEL2_SIZE, LEVEL2_SHIFT, TABLES_PER_ALLOCATION)
  internal val pages = Pages(pagesPerAllocation)  // level 3 and level 4
  var cardinality = 0; private set
  var updateCardinalityMillis = 0L

  val bytesAllocated: Long get() = level1Table.capacity() * 4 + level2Tables.bytesAllocated + pages.bytesAllocated

  init {
    repeat(level1Table.capacity()) { level1Table.put(-1) }
    level1Table.position(0)
  }

  operator fun get(index: Int): Boolean {
    val index1 = index ushr LEVEL1_SHIFT
    val index2 = level1Table[index1]
    val index3 = level2Tables.get(tableIndex = index2, index = index)
    return pages.get(pageIndex = index3, index = index)
  }

  fun set(index: Int, value: Boolean) {
    val index1 = index ushr LEVEL1_SHIFT
    val index2 = level1Table[index1]
    val checkedIndex2 = if (index2 == -1) {
      val i = level2Tables.nextTableIndex()
      level1Table[index1] = i
      i
    }
    else {
      index2
    }
    val index3 = level2Tables.get(tableIndex = checkedIndex2, index = index)
    val checkedIndex3 = if (index3 == -1) {
      val i = pages.nextPageIndex()
      level2Tables.set(tableIndex = checkedIndex2, index = index, value = i)
      i
    }
    else {
      index3
    }
    pages.set(pageIndex = checkedIndex3, index = index, value = value)
  }

  fun updateCardinality(): Int {
    val startNanos = TimeUtils.start()
    val cardinality = pages.updateCardinality()
    this.cardinality = cardinality
    updateCardinalityMillis = TimeUtils.stopMillis(startNanos)
    return cardinality
  }

  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    return if (other is SparseBitSet) {
      pages == other.pages && level2Tables == other.level2Tables && level1Table == other.level1Table
    }
    else {
      false
    }
  }

  override fun hashCode(): Int {
    var result = level1Table.hashCode()
    result = 31 * result + level2Tables.hashCode()
    result = 31 * result + pages.hashCode()
    return result
  }

  override fun toString(): String {
    return "${javaClass.simpleName}{" +
        "\ncardinality=$cardinality" +
        ", \nupdateCardinalityMillis=$updateCardinalityMillis" +
        ", \nmbAllocated=%.1f".format(bytesAllocated.toFloat() / (1024 * 1024)) +
        ", \npages=$pages" +
        ", \nlevel2Tables=$level2Tables" +
        ", \nlevel1Table.capacity=${level1Table.capacity()}" +
        "}"
  }

  companion object : KLogging() {
    const val LEVEL1_BITS = 13
    const val LEVEL2_BITS = 13
    const val PAGE_BITS = 0  // level 3
    const val LEVEL1_SIZE = 1 shl LEVEL1_BITS
    const val LEVEL2_SIZE = 1 shl LEVEL2_BITS
    const val PAGE_SIZE = 1 shl PAGE_BITS
    const val LEVEL2_SHIFT = PAGE_BITS + 6
    const val LEVEL1_SHIFT = LEVEL2_BITS + LEVEL2_SHIFT
    val TABLES_PER_ALLOCATION = 1 shl max(LEVEL1_BITS, LEVEL2_BITS)
    init {
      assert(LEVEL1_BITS + LEVEL2_BITS + PAGE_BITS + 6 == 32) { "bits must add up to 32" }
      logger.info { "SparseBitSet: descAlloc=$descAlloc" }
      logger.info { "SparseBitSet: descBits=$descBits" }
    }

    val descAlloc: String = "{" +
        "TABLES_PER_ALLOCATION=$TABLES_PER_ALLOCATION" +
        "}"

    val descBits: String = "{" +
        "LEVEL1_BITS=$LEVEL1_BITS" +
        ", LEVEL2_BITS=$LEVEL2_BITS" +
        ", PAGE_BITS=$PAGE_BITS" +
        ", LEVEL1_SIZE=$LEVEL1_SIZE" +
        ", LEVEL2_SIZE=$LEVEL2_SIZE" +
        ", PAGE_SIZE=$PAGE_SIZE" +
        ", LEVEL2_SHIFT=$LEVEL2_SHIFT" +
        ", LEVEL1_SHIFT=$LEVEL1_SHIFT" +
        "}"
  }
}

internal class Tables(val tableSize: Int, val shift: Int, val tablesPerAllocation: Int) {
  val arenas = ArrayList<IntBuffer>()  // memory allocation of contiguous regions
  val tables = ArrayList<IntBuffer>(tablesPerAllocation)  // slices/views of the underlying memory regions
  var size = 0; private set  // virtual size
  var allocateMillis = 0L

  val bytesAllocated: Long get() = arenas.fold(0L) { total, arena -> total + arena.capacity() * 4 }

  init {
    allocate()
  }

  fun get(tableIndex: Int, index: Int): Int {
    return if (tableIndex == -1) {
      -1
    }
    else {
      val i = (index ushr shift) and (tableSize - 1)
      tables[tableIndex][i]
    }
  }

  fun set(tableIndex: Int, index: Int, value: Int) {
    val table = tables[tableIndex]
    val i = (index ushr shift) and (tableSize - 1)
    table[i] = value
  }

  fun nextTableIndex(): Int {
    if (size == tables.size) {
      allocate()
    }
    return size++
  }

  private fun allocate() {
    allocateMillis += TimeUtils.millis {
      val arena = ByteBuffer.allocateDirect(tablesPerAllocation * tableSize * 4).asIntBuffer()
      repeat(arena.capacity()) { arena.put(-1) }
      arena.position(0)
      arenas.add(arena)
      repeat(tablesPerAllocation) { i ->
        val position = i * tableSize
        arena.position(position)
        arena.limit(position + tableSize)
        val table = arena.slice()
        table.limit(tableSize)
        tables.add(table)
      }
    }
  }

  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    return if (other is Tables) {
      size == other.size && tables == other.tables
    }
    else {
      false
    }
  }

  override fun hashCode(): Int {
    var result = arenas.hashCode()
    result = 31 * result + tables.hashCode()
    result = 31 * result + size
    return result
  }

  override fun toString(): String {
    return "${javaClass.simpleName}{" +
        "arenas.size=${arenas.size}" +
        ", tables.size=${tables.size}" +
        ", size=$size" +
        ", mbAllocated=%.1f".format(bytesAllocated.toFloat() / (1024 * 1024)) +
        ", allocateMillis=$allocateMillis" +
        "}"
  }
}

internal class Pages(val pagesPerAllocation: Int) {
  val arenas = ArrayList<LongBuffer>()  // memory allocation of contiguous regions
  var pages: Array<Page> = emptyArray()  // slices/views of the underlying memory regions
  var size = 0; private set  // virtual size
  var cardinality: Int = 0; private set
  var allocateMillis = 0L

  val bytesAllocated: Long get() = arenas.fold(0L) { total, arena -> total + arena.capacity() * 8 }

  init {
    allocate()
  }

  fun get(pageIndex: Int, index: Int): Boolean = if (pageIndex == -1) false else pages[pageIndex][index]

  fun set(pageIndex: Int, index: Int, value: Boolean) {
    pages[pageIndex][index] = value
  }

  fun nextPageIndex(): Int {
    if (size == pages.size) {
      allocate()
    }
    return size++
  }

  private fun allocate() {
    allocateMillis += TimeUtils.millis {
      val arena = ByteBuffer.allocateDirect(pagesPerAllocation * SparseBitSet.PAGE_SIZE * 8).asLongBuffer()
      repeat(arena.capacity()) { arena.put(0L) }
      arena.position(0)
      arenas.add(arena)
      var nextPageIndex = pages.size
      val nextPages = arrayOfNulls<Page>(nextPageIndex + pagesPerAllocation)
      System.arraycopy(pages, 0, nextPages, 0, nextPageIndex)
      repeat(pagesPerAllocation) { i ->
        val position = i * SparseBitSet.PAGE_SIZE
        arena.position(position)
        arena.limit(position + SparseBitSet.PAGE_SIZE)
        val bits = arena.slice()
        bits.limit(SparseBitSet.PAGE_SIZE)
        nextPages[nextPageIndex++] = Page(bits)
      }
      @Suppress("UNCHECKED_CAST")
      pages = nextPages as Array<Page>
    }
  }

  fun updateCardinality(): Int {
    var cardinality = 0
    for (i in 0 until size) {
      cardinality += pages[i].cardinality
    }
    this.cardinality = cardinality
    return cardinality
  }

  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    return if (other is Pages) {
      size == other.size && cardinality == other.cardinality && pages.contentEquals(other.pages)
    }
    else {
      false
    }
  }

  override fun hashCode(): Int {
    var result = arenas.hashCode()
    result = 31 * result + pages.hashCode()
    result = 31 * result + size
    result = 31 * result + cardinality
    return result
  }

  override fun toString(): String {
    return "${javaClass.simpleName}{" +
        "arenas.size=${arenas.size}" +
        ", pages.size=${pages.size}" +
        ", size=$size" +
        ", cardinality=$cardinality" +
        ", mbAllocated=%.1f".format(bytesAllocated.toFloat() / (1024 * 1024)) +
        ", allocateMillis=$allocateMillis" +
        "}"
  }
}

internal class Page(val bits: LongBuffer) {
  val cardinality: Int get() {
    var total = 0
    for (i in 0 until SparseBitSet.PAGE_SIZE) {
      total += java.lang.Long.bitCount(bits[i])
    }
    return total
  }

  operator fun get(index: Int): Boolean {
    val i = (index ushr 6) and (SparseBitSet.PAGE_SIZE - 1)
    val word = bits[i]  // word in the sense of x86-64 words
    val n = index and 63
    return (word ushr n) == 1L
  }

  operator fun set(index: Int, value: Boolean) {
    val i = (index ushr 6) and (SparseBitSet.PAGE_SIZE - 1)
    val word = bits[i]  // word in the sense of x86-64 words
    val n = index and 63
    val x = if (value) 1L else 0L  // should translate to a CMOV
    bits[i] = (word and (1L shl n).inv()) or (x shl n)
  }

  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    return if (other is Page) {
      cardinality == other.cardinality && bits == other.bits
    }
    else {
      false
    }
  }

  override fun hashCode(): Int {
    var result = bits.hashCode()
    result = 31 * result + cardinality
    return result
  }

  override fun toString(): String {
    return "${javaClass.simpleName}{" +
        "bits.capacity=${bits.capacity()}" +
        ", cardinality=$cardinality" +
        "}"
  }
}

operator fun ShortBuffer.set(index: Int, value: Short) {
  put(index, value)
}

operator fun IntBuffer.set(index: Int, value: Int) {
  put(index, value)
}

operator fun LongBuffer.set(index: Int, value: Long) {
  put(index, value)
}