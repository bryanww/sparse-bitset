package com.arcrocket

import com.arcrocket.utils.BitSetUtils
import com.zaxxer.sparsebits.SparseBitSet

/**
 * Extends the zaxxer library sparse bitset implementation to expose protected members.
 */
class ZaxxerBitSet : SparseBitSet(Integer.MAX_VALUE) {
  val pages: Array<out Array<LongArray?>?> get() = bits

  // First Test: store all words and the differences between bitset indices as dense arrays
  var words: LongArray = longArrayOf()
  var indexDeltas: IntArray = intArrayOf()

  // Second Test: unpack all bits to their individual indices and pack the differences between all bit indices
  var packedDeltas: LongArray = longArrayOf()

  fun buildWordsAndIndexDeltas() {
    val cardinality = cardinality()
    val words = LongArray(cardinality)
    val indexDeltas = IntArray(cardinality - 1)
    var prevIndex: Int = 0
    var w = 0; var d = 0
    val level1 = pages
    for (i1 in level1.indices) {
      val level2 = level1[i1] ?: continue
      for (i2 in level2.indices) {
        val level3 = level2[i2] ?: continue
        for (i3 in level3.indices) {
          val word = level3[i3]
          if (word == 0L) continue
          words[w++] = word
          val baseIndex =
            (i3 shl 6) or  // make room for bit addresses inside word
            (i2 shl (LEVEL3 + 6)) or  // make room for L3+word
            (i1 shl (LEVEL2 + LEVEL3 + 6))  // make room for L2+L3+word
          if (prevIndex != 0) {
            indexDeltas[d++] = baseIndex - prevIndex
          }
          prevIndex = baseIndex
        }
      }
    }
    this.words = words
    this.indexDeltas = indexDeltas
  }

  fun buildPackedDeltas() {
    val deltas = IntArray(cardinality() - 1)
    var prevIndex: Int = 0
    var d = 0
    val level1 = pages
    for (i1 in level1.indices) {
      val level2 = level1[i1] ?: continue
      for (i2 in level2.indices) {
        val level3 = level2[i2] ?: continue
        for (i3 in level3.indices) {
          var word = level3[i3]; var b = 0
          while (word != 0L) {
            if (word and 1L != 0L) {
              val index = b or
                (i3 shl 6) or  // make room for bit addresses inside word
                (i2 shl (LEVEL3 + 6)) or  // make room for L3+word
                (i1 shl (LEVEL2 + LEVEL3 + 6))  // make room for L2+L3+word
              if (prevIndex != 0) {
                deltas[d++] = index - prevIndex
              }
              prevIndex = index
            }
            ++b
            word = word ushr 1
          }
        }
      }
    }
    this.packedDeltas = BitSetUtils.toLongArray(deltas)
  }

  fun contentDeepEquals(other: ZaxxerBitSet) = pages.contentDeepEquals(other.pages)

  fun equalsByWordsAndIndexDeltas(other: ZaxxerBitSet) = words.contentEquals(other.words) && indexDeltas.contentEquals(other.indexDeltas)

  fun equalsByPackedDeltas(other: ZaxxerBitSet) = packedDeltas.contentEquals(other.packedDeltas)

  val descWordsAndIndexDeltas: String get() = "{words.size=${words.size}, indexDeltas.size=${indexDeltas.size}, cardinality=${cardinality()}}"

  val descPackedDeltas: String get() = "{packedDeltas.size=${packedDeltas.size}, cardinality=${cardinality()}}"

  override fun toString(): String {
    return "${javaClass.simpleName}{" +
        "words.size=${words.size}" +
        ", indexDeltas.size=${indexDeltas.size}" +
        ", packedDeltas.size=${packedDeltas.size}" +
        ", cardinality=${cardinality()}" +
        ", size=${size()}" +
        ", length=${length()}" +
        ", statistics:\n${statistics()}" +
        "}"
  }
}