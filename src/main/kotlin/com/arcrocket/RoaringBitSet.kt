package com.arcrocket

import com.arcrocket.utils.BitSetUtils
import org.roaringbitmap.RoaringBitmap
import java.lang.IllegalStateException

/**
 * Wraps an instance of Roaring Bitmap for testing.
 */
class RoaringBitSet {
  val roaring = RoaringBitmap()
  val cardinality: Int get() = roaring.cardinality
  val sizeInBytes: Int get() = roaring.sizeInBytes
  val serializedSizeInBytes: Int get() = roaring.serializedSizeInBytes()

  // pack the differences between all bit indices
  var packedDeltas: LongArray = longArrayOf()

  fun add(value: Int) = roaring.add(value)
  fun flip(value: Int) = roaring.flip(value)

  fun buildPackedDeltas() {
    val longCardinality = roaring.longCardinality
    if (longCardinality >= Integer.MAX_VALUE) {
      throw IllegalStateException("unexpected cardinality")
    }
    val deltas = IntArray(cardinality - 1)
    var prevIndex: Int = 0
    var d = 0
    for (index in roaring) {
      if (prevIndex != 0) {
        deltas[d++] = index - prevIndex
      }
      prevIndex = index
    }
    this.packedDeltas = BitSetUtils.toLongArray(deltas)
  }

  override fun hashCode(): Int = roaring.hashCode()

  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    return if (other is RoaringBitSet) {
      roaring == other.roaring
    }
    else {
      false
    }
  }

  fun equalsByPackedDeltas(other: RoaringBitSet) = packedDeltas.contentEquals(other.packedDeltas)

  val descPackedDeltas: String get() = "{packedDeltas.size=${packedDeltas.size}, cardinality=${cardinality}}"

  override fun toString(): String {
    return "${javaClass.simpleName}{" +
        "packedDeltas.size=${packedDeltas.size}" +
        ", cardinality=${cardinality}" +
        ", sizeInBytes=${sizeInBytes}" +
        ", serializedSizeInBytes=${serializedSizeInBytes}" +
        "}"
  }
}