# sparse-bitset

Surveys the [sparse bitset implementation](https://github.com/brettwooldridge/SparseBitSet). Looks at both sparse bitset creation and sparse bitset comparison with `equals`. `Hashing.murmur3_32` is used as the hash function.

### Bitset creation

My first thought was to try to improve memory organization by creating my own implementation using memory arenas. The motivation was that the reference library is building a 3D matrix by appending dynamically allocated arrays, and I wanted to see if I could improve on this with singular block allocation which should exhibit better memory locality. I used a simple virtual memory paging scheme similar to the Zaxxer implementation.

My implementation works well, and I used NIO and ByteBuffer slices to isolate the memory to single memory arenas. Performance is very good, but it does not beat the Zaxxer implementation. For example, the output of one run is located in `output/BitSetImplementationTests.txt`:

```
Zaxxer:
testZaxxerEnwik8: loaded "src/test/resources/enwik8": millis=4685, memoryInfo={usedMB=1295.6, totalMB=4096.0}
testZaxxerEnwik8: words.size=13303056
testZaxxerEnwik8: finished hashing: millis=724, memoryInfo={usedMB=1002.9, totalMB=4096.0}
testZaxxerEnwik8: hashes.size=13303056
testZaxxerEnwik8: running test: memoryInfo={usedMB=747.9, totalMB=4096.0}
testZaxxerEnwik8: finished building: millis=829, memoryInfo={usedMB=959.9, totalMB=4096.0}
testZaxxerEnwik8: finished building: result=ZaxxerBitSet{words.size=0, indexDeltas.size=0, packedDeltas.size=0, cardinality=1438900, size=2147480362, length=2147483600, statistics:
Cardinality            = 1438900

Custom:
testSparseEnwik8: loaded "src/test/resources/enwik8": millis=4026, memoryInfo={usedMB=1296.3, totalMB=4096.0}
testSparseEnwik8: words.size=13303056
testSparseEnwik8: finished hashing: millis=732, memoryInfo={usedMB=1002.9, totalMB=4096.0}
testSparseEnwik8: hashes.size=13303056
testSparseEnwik8: running test: memoryInfo={usedMB=747.9, totalMB=4096.0}
testSparseEnwik8: finished building: millis=1938, memoryInfo={usedMB=892.9, totalMB=4096.0}
testSparseEnwik8: finished building: result=SparseBitSet{
cardinality=1439138, 
updateCardinalityMillis=16, 
mbAllocated=271.3, 
pages=Pages{arenas.size=1, pages.size=1995458, size=1423831, cardinality=1439138, mbAllocated=15.2, allocateMillis=101}, 
level2Tables=Tables{arenas.size=1, tables.size=8192, size=8192, mbAllocated=256.0, allocateMillis=258}, 
level1Table.capacity=8192}
```

This is a test of input text file `enwik8`, [zip found here](http://mattmahoney.net/dc/textdata.html). Note that the Zaxxer implementation does not handle negative int values so negative values are negated. This slightly reduces the cardinality. The custom implementation handles negative values. As long as the value is negated before calling the hash function there is no difference between using a conditional statement compared to 2s complement and shift since the former will translate to a CMOV instruction; just be careful that the hashing function call is not inside the conditional block.

Based on the JVM Heap profile, the Zaxxer bitset is using ~211MB of heap memory, and my custom implementation is using ~271MB of native memory. Zaxxer built the bitset in 829ms while my custom implementation built it in 1938ms, with ~359ms spent on memory allocation. The JRE 11 JVM heap allocator is highly optimized and handles the Zaxxer use case extremely well.

Given the time I could probably make the custom implementation perform as well as the Zaxxer implementation by:

*  using the heap allocator the same way the Zaxxer implementation does.
*  running a warmup routine to get the Hotspot to inline the bitset `set` methods ahead of time; Zaxxer avoids this by "inlining" a single method named `setScanner`.

The custom implementation has several advantages:

*  It is a much simpler implementation, so it is much easier to port to other languages, can be modified much more easily, and is much more maintainable.
*  It is easier to modify since you can simply change `LEVEL1_BITS`, `LEVEL1_BITS`, and `PAGE_BITS`; this could also be changed dynamically at runtime if the input is profiled.
*  Direct memory management will possibly perform better in a native language such as C/C++ or Rust.
*  Function inlining is easier to control in languages such as C/C++ or Rust.

### Bitset comparison

I then took a look at bitset comparion using the `equals` method. I retained my idea that operating on the Zaxxer 3D matrix likely exhibits poor locality.

I first profiled the data in the case of `enwik8` by looking at the index deltas. I found that, even though we use long values to represent packed bits, the flags are extremely sparse:

````
testDeltas; deltas: size=1439137, min=1, max=43599, average=2984.403161061108
````

In the 2^32 bit address space, for this use case the flags are separated by an average distance of 3000, i.e. 2^11 to 2^12, or 11-12 bits apart. Since the packed bitset words are 2^6 bits in length, most of these flags are the only flag stored inside the word. This makes sense because by using the Murmur hash, we are effectively transforming the distribution of the input values from a possibly clustered distribution to something much closer to a Uniform distribution (it should be noted here that cardinality is thus an approximation and not an exact value due to hash collisions of different input text).

Since we use bit addressing for array lookup, the data structure has a non-decreasing proper ordering. This means that packing the information into properly ordered arrays can be used to compare them. Packing the information into arrays is a Data-Oriented Design approach that exploits CPU cache locality. I tried two approaches:

1.  WordsAndIndexDeltas: Pack all words (longs) and pack the deltas of their respective base-indices (ints); compare both the array of words and the array of base-indices.
2.  PackedDeltas: pack the deltas of the bit-indices into long-values; compare the deltas.

Results on `enwik8`:

```
testZaxxerEquals: equals x 1000: millis=142606, memoryInfo={usedMB=2089.5, totalMB=4096.0}
testZaxxerEqualsByWordsAndIndexDeltas: equalsByWordsAndIndexDeltas x 1000: millis=6817, memoryInfo={usedMB=1000.1, totalMB=4096.0}
testZaxxerEqualsByPackedDeltas: equalsByPackedDeltas x 1000: millis=1496, memoryInfo={usedMB=567.3, totalMB=4096.0}
```

Additional memory:

```
WordsAndIndexDeltas: words.size=1438900, indexDeltas.size=1438899 (~16.5MB)
PackedDeltas: packedDeltas.size=719450 (~5.4MB)
```

The reason I pack the index deltas into long-values in `PackedDeltas` is because the word size on x86-64 is 64-bits, so it should be faster to compare long-values than 2 int values (unless the compiler can spot this).

For comparision of bitsets with packed deltas, **this is a factor of improvement of ~95, or 2 orders of magnitude faster**. The words and indices comparison is an improvement of ~20, so the results indicate that CPU cache locality is indeed leveraged in these cases.

Compared to the runtime of one Zaxxer `equals` operation, packing the data is within the same order of magnitude:

```
testZaxxerEquals: equals: millis=170, memoryInfo={usedMB=2088.0, totalMB=4096.0}
testZaxxerEqualsByWordsAndIndexDeltas: buildWordsAndIndexDeltas0: millis=256, memoryInfo={usedMB=946.0, totalMB=4096.0}
testZaxxerEqualsByPackedDeltas: buildPackedDeltas0: millis=321, memoryInfo={usedMB=2037.0, totalMB=4096.0}
```

This means the above solution would be a dramatic improvement over the base use of `equals`, and can be extended to intersection-type queries because it retains a proper ordering of the information.
